# NativeAutomationBridge

## What this package is doing

Exploring new ways to do Automation Testing in Unity/Games. This package tries to leverage the already existing and powerful automation frameworks on the native platform for Unity. The main feature is the creation and synchronization of a native UI layer on top of the Unity Activity to emulate a native UI which is best understood by native automation frameworks.

## How to setup

To have the code active in your Android app you have to specify a custom activity class name. Change this in your AndroidManifest.xml:

`<activity android:name="com.robinbirdstudios.nativeautomationbridge.TestUnityPlayerActivity">` or if you are already using a custom Activity inherit from the `TestUnityPlayerActivity`

Android: Have the `NativeAutomationBridge-release.aar` Android Extension in your Unity project and configure the Import Settings to include it in Android builds.

Check out the Example project in this repository to see the setup.

## Example

The Unity UI example scene featuring three buttons in Unity UI

![Unity UI](/Attic/ReadmeAssets/UnityUIPicture.png)

The areas in pink which will be used to create native Android/iOS Buttons at runtime which can be detected by native automation frameworks like [Espresso](https://developer.android.com/training/testing/espresso/) or [XCTests](https://developer.apple.com/documentation/xctest)

![Native UI](/Attic/ReadmeAssets/NativeUIPictureHighlight.png)


## How to Build

### Android

Open `/AnroidNative` in Android Studio and execute `copyJarToUnity` to copy result .aar file into neighbour Unity project. Change paths as needed for own project.


### iOS

Coming soon™