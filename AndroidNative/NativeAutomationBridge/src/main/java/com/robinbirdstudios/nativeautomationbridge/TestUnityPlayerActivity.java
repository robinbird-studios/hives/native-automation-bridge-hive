package com.robinbirdstudios.nativeautomationbridge;

import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestUnityPlayerActivity extends UnityPlayerActivity {

    private AbsoluteLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (this.mUnityPlayer != null) {
            this.mUnityPlayer.quit();
            this.mUnityPlayer = null;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);


        mUnityPlayer = new UnityPlayer(this);

        setContentView(mUnityPlayer);
        mUnityPlayer.requestFocus();
    }

    public boolean IsTestLabTest() {
        return true;
//        String testLabSetting = Settings.System.getString(getContentResolver(), "firebase.test.lab");
//        if ("true".equals(testLabSetting)) {
//            return true;
//        }
//        return false;
    }

    public void CreateButton(DataClass[] datas) {

        TestUnityPlayerActivity context = this;
        runOnUiThread(() -> {

            if (layout == null)
            {
                layout = new AbsoluteLayout(context);
                layout.setMinimumHeight(0);

                mUnityPlayer.addView(layout);
            }
            else
            {
                layout.removeAllViews();
            }

            for (int i = 0; i < datas.length; i++) {
                // Stuff that updates the UI
                DataClass data = datas[i];
                Button myButton = new Button(context);

                myButton.setPivotX(0);
                myButton.setPivotY(0);
                myButton.setMinimumHeight(0);
                myButton.setMinimumWidth(0);
                myButton.setTextSize(1);
                myButton.setX(data.x);
                myButton.setY(layout.getHeight() - data.y - data.height);
                myButton.setMinWidth(0);
                myButton.setWidth(data.width);
                myButton.setMinHeight(0);
                myButton.setHeight(data.height);
                myButton.setTag(data.name);
                myButton.setText(data.name);

                myButton.getBackground().setAlpha(128);
                myButton.setOnClickListener(v -> UnityPlayer.UnitySendMessage("FirebaseTestLab", "UiCreatorButtonPressed", data.name));
                layout.addView(myButton);
            }
        });


    }
}
