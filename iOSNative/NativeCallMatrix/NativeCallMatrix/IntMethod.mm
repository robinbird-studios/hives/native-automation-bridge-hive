#import <Foundation/Foundation.h>


extern "C" void _IntMethod(int myInt)
{
    NSNumber* myNumber = [NSNumber numberWithInt:myInt];
    NSLog(@"Got int number: %@", myNumber);
}
